from flask_restplus import Namespace

class BaseModelSchema:
    authorizations = {
        'apikey': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'X-API-KEY'
        },
    }

    @classmethod
    def create_namespace(cls, name, description):
        return Namespace(name, description=description,
                    authorizations=BaseModelSchema.authorizations)