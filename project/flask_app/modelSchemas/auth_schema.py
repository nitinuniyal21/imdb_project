from flask_restplus import Namespace, fields


class AuthSchema:
    api = Namespace('auth', description='Authenticatin related operations')
    auth_user = api.model('auth', {
        'email': fields.String(required=True, description="Your email address"),
        'password': fields.String(required=True, description="Your password")
    })
