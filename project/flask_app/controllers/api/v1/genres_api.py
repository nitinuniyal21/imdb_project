from flask_restplus import Resource
from project.flask_app.models.genre import Genre
from project.flask_app.modelSchemas.genre_schema import GenreSchema
from project.flask_app.modelSchemas.movie_schema import MovieSchema
from project.flask_app.utils.decorators import admin_login_required

api = GenreSchema.api
genre_schema = GenreSchema.genres
movie_schema = MovieSchema.movie

@api.route('/')
class Genres(Resource):
    @api.doc('list of genres')
    @api.marshal_list_with(genre_schema, envelope="data")
    def get(self):
        return Genre.get_all_genres()

    @admin_login_required
    @api.doc('create genre', security="apikey")
    @api.expect(genre_schema, validate=True)
    def post(self):
        data = api.payload
        return GenreSchema.format_and_save(data)

@api.route('/<id>')
class GenreDetail(Resource):

    @admin_login_required
    @api.doc('edit genre name')
    @api.expect(genre_schema, validate=True)
    def put(self, id):
        genre = Genre.get_genre(id)
        if not genre:
            api.abort(404)
        data = api.payload
        return GenreSchema.format_edit_save(genre, data)


@api.route('/<id>/movies')
class GenreMovies(Resource):

    @api.doc(description='list of movies')
    @api.marshal_list_with(movie_schema, description="list of movies", envelope="data")
    def get(self, id):
        genre = Genre.get_genre(id)
        if not genre:
            api.abort(404)
        return genre.get_movies(), 200    