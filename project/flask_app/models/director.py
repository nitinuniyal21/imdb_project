from project.extensions import db
from .movie import Movie


class Director(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100))
    movies = db.relationship(Movie, backref='director', lazy='dynamic')

    @classmethod
    def create_director(cls, name):
        name = " ".join(map(lambda x: x.title(), name.split(" ")))
        director = cls.query.filter_by(name = name).first()
        if not director:
            new_director = Director(
                name=name
            )
            db.session.add(new_director)
            db.session.commit()
            return {
                'status': 'success',
                'message': 'New directory added successfully',
                'director': new_director
            }, 201
        else:
            return {
                'status': 'error',
                'message': 'Directory already added'
            }, 409

    @classmethod
    def get_or_create_director(cls, name):
        name = " ".join(map(lambda x: x.title(), name.split(" ")))
        director = cls.query.filter_by(name = name).first()
        if director:
            return {
                'status': 'success',
                'message': 'Pre present director returned',
                'director': director
            }, 200
        else:
            return cls.create_director(name)

    @classmethod
    def get_director_by_name(cls, name):
        name = " ".join(map(lambda x: x.title(), name.split(" ")))
        director = cls.query.filter_by(name = name).first()
        if director:
            return {
                'status': 'success',
                'message': 'directory exists',
                'director': director
            }, 200
        else:
            return {
                'status': 'error',
                'message': 'No result found'
            }, 404

    def add_movie(self, movie):
        if isinstance(movie, Movie):
            self.movies.append(movie)
            return {
                'status': 'success',
                'message': 'Movie added successfully'
            }, 203
            
        else:
            return {
                'status': 'error',
                'message': 'Invalid movie, movie is not an instance of Movie'
            }, 409
    
    def remove_movie(self, movie):
        if isinstance(movie, Movie):
            self.movies.remove(movie)
            return {
                'status': 'success',
                'message': 'Movie added successfully'
            }, 200
        else:
            return {
                'status': 'error',
                'message': 'Invalid movie, movie is not an instance of Movie'
            }, 409
    
    def remove_all_movies(self):
        self.movies = []

    def get_movies(self):
        return self.movies.all()

    def save_data(self, data= None):
        data = data if data else self 
        db.session.add(data)
        db.session.commit()

    @classmethod
    def get_all_directors(cls):
        return cls.query.all()

    def __repr__(self):
        return f'{self.name}'
