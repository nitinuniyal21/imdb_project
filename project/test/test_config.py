import os
import unittest

from flask import current_app
from flask_testing import TestCase
from manage import app
from project.config.base_config import basedir


class TestDevelopmentConfig(TestCase):

    def create_app(self):
        app.config.from_object("project.config.dev_config.DevlopmentConfig")
        return app

    def test_app_is_development(self):
        self.assertTrue(app.config['SECRET_KEY'] is 'some_random_secret_key')
        self.assertTrue(app.config['DEBUG'] is True)
        self.assertFalse(current_app is None)
        self.assertTrue(
            app.config['SQLALCHEMY_DATABASE_URI'] ==
            'sqlite:///' + os.path.join(basedir, 'app.db')
        )


class TestTestingConfig(TestCase):

    def create_app(self):
        app.config.from_object("project.config.test_config.TestingConfig")
        return app

    def test_app_is_testing(self):
        self.assertTrue(app.config['SECRET_KEY'] is 'some_random_secret_key')
        self.assertTrue(app.config['DEBUG'] is True)
        self.assertTrue(app.config['TESTING'] is True)
        self.assertTrue(
            app.config['SQLALCHEMY_DATABASE_URI'] == 'sqlite:///' +
            os.path.join(basedir, 'app_test.db')
        )


class TestProductionConfig(TestCase):
    def create_app(self):
        app.config.from_object('project.config.prod_config.ProductionConfig')
        return app

    def test_app_is_production(self):
        self.assertTrue(app.config['DEBUG'] is False)


if __name__ == '__main__':
    unittest.main()
